'''
Created on 11/11/2021

@author: Herrera
'''

# Fancy That PDF. 2021. ESTRUCTURA DE DATOS EN C LUIS JOYANES AGUILAR PDF. 
# [online] Available at: <https://fancythatcakes.net/estructura-de-datos-en-c-luis-joyanes-aguilar-50/> 


# Implementado de java a python

class VerticeLista:
    def __init__(self, x):
        self.nombre = x
        self.numVertice = -1
        self.lad = []
        
    def equals(self, d):
        dos = d
        return self.nombre == dos.nombre
    
    def asigVert(self, n):
        self.numVertice = n
        
    def nomVertice(self):
        return self.nombre
    
    def __str__(self):
        return f"{self.nombre} ({self.numVertice})"
    

class Arco:
    
    def __init__(self, d):
        self.destino = d
        self.peso = 0
    
    def getDestino(self):
        return self.destino
    
    def equals(self, n):
        a = n
        return self.destino == a.destino
    
    def __str__(self):
        return f"Arco [ destino = {self.destino} peso = {self.peso} ]"
    
class GrafoLista:
    
    def __init__(self, mx):
        self.numVerts = 0
        self.maxVerts = mx
        self. tablaAdc = []
        
    def vertices(self):
        return self.tablaAdc
    
    def listaAdyc(self, v):
        if(v < 0 or v>= self.numVerts):
            raise Exception ("\nVertice fuera de rango")
        return self.tablaAdc[v].lad
    
    def numVertice(self, nombre):
        v = VerticeLista(nombre)
        encontrado = False
        i = 0
        while not encontrado and i < self.numVerts:
            encontrado = self.tablaAdc[i].equals(v)
            if not encontrado:
                i+=1
        
        if i < self.numVerts:
            return i
        else:
            return -1
        
    def nuevoVertice(self, nom):
        esta = self.numVertice(nom) >= 0
        if not esta:
            v = VerticeLista(nom)
            v.asigVert(self.numVerts)
            self.tablaAdc.append(v)
            self.numVerts+=1
            print("\nSe agrego el vertice")
            
    def adyacente(self, a, b):
        v1 = v2 = 0
        v1 = self.numVertice(a)
        v2 = self.numVertice(b)
        
        if(v1 <0 or v2 < 0):
            raise Exception("\nEl vertice no existe")
        
        if (Arco(v2) in self.tablaAdc[v1].lad):
            return True
        else:
            return False
            
    def nuevoArco(self, a, b):
        if(not self.adyacente(a, b)):
            v1 = self.numVertice(a)
            v2 = self.numVertice(b)
            if(v1<0 or v2<0):
                raise Exception("\nEl vertice no existe")
            
            ab = Arco(v2)
            self.tablaAdc[v1].lad.insert(0, ab)
            print("\nSe agrego la arista")
            
    def borrarArco(self, a, b):
        v1 = self.numVertice(a)
        v2 = self.numVertice(b)
        if(v1<0 or v2<0):
            raise Exception("\nEl vertice no existe")
        ab = Arco(v2)
        for i in self.tablaAdc[v1].lad:
            
            if ab.destino == i.destino:
                self.tablaAdc[v1].lad.remove(i)
                print("hi")
            
        
        print("\nArista eliminada")
        
class RecorridoLista:
    clave = 9999
    def __init__(self):
        pass
    
    @staticmethod
    def recorridoAnchura(g, org):
        v = w = 0
        cola = []
        m = []
        
        v = g.numVertice(org)
        if(v<0):
            raise Exception("Verice no existe")
        
        for i in range(0, g.numVerts):
            m.append(RecorridoLista.clave)
            
        m[v] = 0 
        cola.append(v)
        
        while((cola)):
            cw = 0
            cw = cola.pop()
            w = cw
            print(f"Vertice {g.tablaAdc[w]} visitado")
            
            list = iter(g.tablaAdc[w].lad)
            ck = Arco
            
            while(True):
                k = 0
                try:
                    ck = next(list)
                except:
                    ck = None
                
                if(ck):
                    k = ck.getDestino()
                    if (m[k] == RecorridoLista.clave):
                        cola.append(k)
                        m[k] = 1
                
                if ck == None:
                    break
                
    @staticmethod
    def recorridoProfundidad(g, org):
        v = w = 0
        pila = []
        m = []
        
        v = g.numVertice(org)
        if(v<0):
            raise Exception("Verice no existe")
        
        for i in range(0, g.numVerts):
            m.append(RecorridoLista.clave)
            
        m[v] = 0 
        pila.append(v)
        
        while((pila)):
            cw = 0
            cw = pila.pop()
            w = cw
            print(f"Vertice {g.tablaAdc[w]} visitado")
            
            list = iter(g.tablaAdc[w].lad)
            ck = Arco
            
            while(True):
                k = 0
                try:
                    ck = next(list)
                except:
                    ck = None
                
                if(ck):
                    k = ck.getDestino()
                    if (m[k] == RecorridoLista.clave):
                        pila.append(k)
                        m[k] = 1
                
                if ck == None:
                    break
        
        
        
grafo = GrafoLista(100)

while True:
    print("\nElige una de las siguientes opciones")
    print("1) Agregar vertice")
    print("2) Agregar arco")
    print("4) Verificar si 2 vertices son adyacentes")
    print("5) Eliminar arista")
    print("6) Recorrido profundidad")
    print("7) Recorrido en anchura")
    print("8) Salir")
    try:
        opcion = int(input("Introduce opcion: "))
        if (opcion == 1):
            vertice = input("\nIntroduce vertice: ")
            grafo.nuevoVertice(vertice)

        elif (opcion == 2):
            
            try:
                v1 = input("\nIntroduce primer vertice: ")
                v2 = input("\nIntroduce segundo vertice: ")
            
                grafo.nuevoArco(v1, v2)
            except Exception as e:
                print("\nNo existen los vertices")
            
        elif (opcion == 4):
            
            v1 = input("\nIntroduce primer vertice: ")
            v2 = input("\nIntroduce segundo vertice: ")
            try:
                print(grafo.adyacente(v1, v2))
            except Exception:
                print("\nError en los vertices")
        
        elif (opcion == 5):
            vertice = input("\nIntroduce primer vertice: ")
            vertice2 = input("\nIntroduce segundo vertice: ")
            grafo.borrarArco(v1, v2)
            
        elif (opcion == 6):
            v = input("\nIntroduce vertice: ")
            RecorridoLista.recorridoProfundidad(grafo, v)
            
        elif (opcion == 7):
            v = input("\nIntroduce vertice: ")
            RecorridoLista.recorridoAnchura(grafo, v)
            
        elif (opcion == 8):
            print("\nPrograma terminado")
            break
            
        else:
            print("\nOpcion incorrecta")

    except ValueError as e:
        print(f"Error <{e}>")
    print()
            
         
