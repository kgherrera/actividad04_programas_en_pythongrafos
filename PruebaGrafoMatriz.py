'''
Created on 10/11/2021

@author: Herrera
'''

# Fancy That PDF. 2021. ESTRUCTURA DE DATOS EN C LUIS JOYANES AGUILAR PDF. 
# [online] Available at: <https://fancythatcakes.net/estructura-de-datos-en-c-luis-joyanes-aguilar-50/> 

# Implementado de java a python

class Cola:
    def __init__(self):
        self.items = []

    def estaVacia(self):
        return self.items == []

    def agregar(self, item):
        self.items.insert(0,item)

    def avanzar(self):
        return self.items.pop()

    def tamano(self):
        return len(self.items)

class Vertice:
    def __init__(self, nombre):
        self.nombre = nombre
        self.__numVertice = -1

    @property
    def numVertice(self):
        return self.__numVertice
    
    @numVertice.setter
    def numVertice(self, numVertice):
        self.__numVertice = numVertice

    def equals(self, n):
        return self.nombre == n.nombre

    def asigVert(self, n):
        self.__numVertice = n

    def __str__(self):
        return f"{self.nombre} ({self.numVertice})"

class GrafoMatriz:

    maxVerts = 0

    def __init__(self, max):
        self.verts = []
        self.matAd = []
        self.numVerts = 0
        
        self.llenar(max)

    def llenar(self, max):
        for i in range(max):
            self.matAd.append([])
            for j in range(max):
                self.matAd[i].append(0)

    def imprimirFormato(self):
        print("\nMatriz")
        for i in range(len(self.matAd[0])):
            for j in range(len(self.matAd[0])):
                print(f"{self.matAd[i][j]} ", end="")
            print()
    
    def nuevoVertice(self, nom):
        esta = self.numVertice(nom) >= 0

        if not esta:
            v = Vertice(nom)
            v.asigVert(self.numVerts)
            self.verts.append(v)
            self.numVerts+=1
            print("\nSe agrego el vertice")

    def numVertice(self, vs):
        v = Vertice(vs)
        encontrado = False
        i = 0

        while not encontrado and i < self.numVerts:
            encontrado = self.verts[i].equals(v)
            if not encontrado:
                i+=1
        
        if i < self.numVerts:
            return i
        else:
            return -1

    def nuevoArco(self, a, b):
        va = self.numVertice(a)
        vb = self.numVertice(b)

        if (va < 0 or vb < 0):
            raise Exception ("\nEl vertice no existe")
        self.matAd[va][vb] = 1
        print("\nSe agrego el arista")
    
    def adyacente(self, a, b):
        va = self.numVertice(a);
        vb = self.numVertice(b);
        
        if (va < 0 or vb < 0):
            raise Exception ("El vertice no existe");
        return self.matAd[va][vb] == 1
    
    def eliminarArista(self, a,b):
        v1 = self.numVertice(a)
        v2 = self.numVertice(b)
        if(v1 >= len(self.verts) or v2 >= len(self.verts) ):
            print("Vertices invalidos")
        elif(self.matAd[v1][v2] == 0):
            print("La arista no existe")
        else:
            self.matAd[v1][v2] = 0
            print("\nSe elimino la arista")
            
    def eliminarVertice(self, nom):
        v1 = self.numVertice(nom)
        if(v1> self.numVerts or v1 <0):
            print("\nVertice no encontrado")
        else:
            for i in range(len(self.verts)):
                self.matAd[v1][i] = 0
            for i in range(len(self.verts)):
                self.matAd[i][v1] = 0
            print("\nVertice eliminado")
            
class Recorrido:
    
    clave = 9999
    def __init__(self):
        pass
    
    @staticmethod
    def recorridoProfundidad(g, org):
        w = v = 0
        m = []
        
        v = g.numVertice(org)
        
        if(v<0):
            raise Exception("\nEl vertice no existe")
        
        pila = []
        
        for i in range(0 , g.numVerts):
            m.append(Recorrido.clave)
            
        m[v] = 0
        pila.append(v)
        
        # Lista vacia = false
        while(pila):
            w = pila.pop()
            print(f"Vertice {g.verts[w]} => visitado")
            
            for u in range(0, g.numVerts):
                if((g.matAd[w][u] == 1) and (m[u] == Recorrido.clave)):
                    m[u] = m[w] + 1
                    pila.append(u)
        return m
    
    @staticmethod
    def recorridoAnchura(g, org):
        w = v = 0
        m = []
        
        v = g.numVertice(org)
        
        if(v<0):
            raise Exception("\nEl vertice no existe")
        
        cola = Cola()
        
        for i in range(0 , g.numVerts):
            m.append(Recorrido.clave)
            
        m[v] = 0
        cola.agregar(v)
        
        # Lista vacia = false
        while(not(cola.estaVacia())):
            w = cola.avanzar()
            print(f"Vertice {g.verts[w]} => visitado")
            
            for u in range(0, g.numVerts):
                if((g.matAd[w][u] == 1) and (m[u] == Recorrido.clave)):
                    m[u] = m[w] + 1
                    cola.agregar(u)
        return m

grafo = GrafoMatriz(100)

while True:
    print("\nElige una de las siguientes opciones")
    print("1) Agregar vertice")
    print("2) Agregar arco")
    print("3) Ver matriz")
    print("4) Verificar si 2 vertices son adyacentes")
    print("5) Eliminar arista")
    print("6) Recorrido profundidad")
    print("7) Recorrido en anchura")
    print("8) Salir")
    try:
        opcion = int(input("Introduce opcion: "))
        if (opcion == 1):
            if grafo.numVerts!=len(grafo.matAd[0]):
                vertice = input("\nIntroduce vertice: ")
                grafo.nuevoVertice(vertice)

            else:
                print("\nNo se agrego")

        elif (opcion == 2):
            
            try:
                v1 = input("\nIntroduce primer vertice: ")
                v2 = input("\nIntroduce segundo vertice: ")
            
                grafo.nuevoArco(v1, v2)
            except Exception as e:
                print("\nNo existen los vertices")

        elif (opcion == 3):
            grafo.imprimirFormato()
            
        elif (opcion == 4):
            
            v1 = input("\nIntroduce primer vertice: ")
            v2 = input("\nIntroduce segundo vertice: ")
            try:
                print(grafo.adyacente(v1, v2))
            except Exception:
                print("\nError en los vertices")
        
        elif (opcion == 5):
            vertice = input("\nIntroduce primer vertice: ")
            vertice2 = input("\nIntroduce segundo vertice: ")
            grafo.eliminarArista(v1, v2)
            
        elif (opcion == 6):
            v = input("\nIntroduce vertice: ")
            Recorrido.recorridoProfundidad(grafo, v)
            
        elif (opcion == 7):
            v = input("\nIntroduce vertice: ")
            Recorrido.recorridoAnchura(grafo, v)
            
        elif (opcion == 8):
            print("\nPrograma terminado")
            break
            
        else:
            print("\nOpcion incorrecta")

    except ValueError as e:
        print(f"Error <{e}>")
    print()